#include "list.h"
#include <iostream>
#include <cstring>
using namespace std;


void readIn(node * & ptr)
{
	char continueCheck = 'y';
	char temp[200];

	node * current = nullptr;

	while(continueCheck == 'y')
	{
		
		node * charTemp = new node;



		cout << "enter challenge: " << endl;
		cin.getline(temp, 200);
		charTemp->clueOne.challenge = new char[strlen(temp) + 1];
		strcpy(charTemp->clueOne.challenge, temp);

		cout << "Enter Hint: " << endl;
		cin.getline(charTemp->clueOne.hint, 100);

		cout << "Enter Answer: " << endl;
		cin.getline(charTemp->clueOne.answer, 100);

		cout << "Enter Difficulty Level (1-3): " << endl;
		cin >> charTemp->clueOne.difficulty;
		cin.ignore(100, '\n');

		
		charTemp->next = nullptr;

		if (ptr == nullptr)
		{
			ptr = charTemp;
			current = charTemp;
		}
		else
		{
			current->next = charTemp;
			current = charTemp;
		}


		cout << "continue? (y/n): " << endl;
		cin >> continueCheck;
		cin.ignore(100,'\n');


	}
}

