#ifndef LIST_H
#define LIST_H
#include <iostream>
#include <cstring>


struct clue
{
	char * challenge;
	char hint[200];
	char answer[200];
	int difficulty;
	int streak;
};


struct node
{
	clue clueOne;
	node * next;
};

void readIn(node *&);

void displayAll(node *);

void userInput(node *);

#endif
