#include <iostream>
#include "list.h"
using namespace std;

/*
------------------------------------------------------------------------------------------------------------------
Name: Rami Hozi
Date: 06/05/2024
class: cs162-009 , Karla Fant
Program 5
Purpose: The purpose of this program is to present a scavenger hunt type game to the user. The first user will have
the chance to create the challenges and hints, while the second player will be able to go in and solve these unique
challenges. This will all be done using a LLL, nodes, and functions to help create this program. The user will also
be able to set the difficulty for the second user.
--------------------------------------------------------------------------------------------------------------------
*/

int main()
{

	node * head = NULL;


	readIn(head);

	userInput(head);

	

	// Freeing Alllllll The Memory
	node * current = head;
	while (current != nullptr)
	{
		node * toDelete = current;
		current = current ->next;
		delete [] toDelete->clueOne.challenge;
		delete toDelete;
	}






	return 0;
}
