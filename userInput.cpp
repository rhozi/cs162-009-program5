#include "list.h"
#include <iostream>
#include <cctype>
#include <cstring>


using namespace std;

void userInput(node * ptr)
{
	char userInput[300];
	node * current = ptr;




	while (current != nullptr)
	{
		cout << "Challenge: " << current->clueOne.challenge << endl;
		do
		{

			cout << "Enter Answer: " << endl; 
			cin.getline(userInput, 100);

			if(strcmp(userInput,current->clueOne.answer) == 0)
			{
				cout << "Correct Answer!" << endl;
			}
			else
			{
				cout << "Wrong! Hint: " << current->clueOne.hint << endl;
			}

		} while(strcmp(userInput,current->clueOne.answer) != 0);

		current = current->next;

	}
}
